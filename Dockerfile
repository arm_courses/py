ARG BASE_CONTAINER=jupyter/all-spark-notebook:spark-2
FROM $BASE_CONTAINER

LABEL maintainer="aRoming <aroming@qq.com>"

USER root

# timezone
RUN apt-get update && \
    apt install -y tzdata

# use orchestrator to set ${TZ}, not in image
# ENV TZ=Asia/Shanghai
# RUN ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

USER $NB_USER

# some python extensions and jupyter extensions
RUN pip3 install \
    xlwt \
    openpyxl \
    emoji \
    jupyter_contrib_nbextensions jupyter_nbextensions_configurator \
    yapf \
    && \
    jupyter contrib nbextension install --user --skip-running-check
