# 《Python程序设计》

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [关于仓库](#关于仓库)
    1. [许可协议](#许可协议)
    2. [编写环境和使用方法](#编写环境和使用方法)
    3. [Jupyter Notebook](#jupyter-notebook)
    4. [帮助完善](#帮助完善)
    5. [组织结构](#组织结构)
2. [关于课程](#关于课程)
    1. [课程基本信息](#课程基本信息)
    2. [课程教材与实验教材](#课程教材与实验教材)
3. [Bibliographies](#bibliographies)
    1. [Books](#books)
    2. [Courses](#courses)
    3. [Tutorials](#tutorials)
    4. [Documentations](#documentations)
    5. [Exercises](#exercises)

<!-- /code_chunk_output -->

## 关于仓库

### 许可协议

本仓库使用`CC-BY-SA-4.0`协议，更详细的协议内容请见<https://gitlab.com/arm_commons/commons/-/blob/master/LICENSES/CC-BY-SA-4.0/README.md>

### 编写环境和使用方法

请详见<https://gitlab.com/arm_commons/commons/-/blob/master/ENVS/MD_RAW/README.md>

### Jupyter Notebook

使用`docker-compose`运行`Jupyter`，本仓库目录映射到`Jupyter container`的工作目录下，具体请见[docker-compose.yaml](./docker-compose.yaml)和[Dockerfile](./Dockerfile)。

如需运行，请在本仓库目录下运行`docker-compose up -d`，然后在浏览器中打开`http://localhost:8888`（无`token`）

### 帮助完善

期待您一起完善，您可以通过以下方式帮助完善：

1. **`merge request`**：通过`GitLab`的`merge request`到`master`分支
1. **`issue`**： 通过`GitLab`或`Gitee`的`issue`发起一个新的`issue`（标签设置成`optimize`）

仓库地址：

1. 主地址： <https://gitlab.com/arm_courses/py>
1. 镜像地址： <https://gitee.com/aroming/course_py>

### 组织结构

1. 目录组织：
    1. **`Addons/`** ： 附加的选读内容，如：专题类的论述等
    1. **`Experiments/`** ： 实验指导及FAQ
    1. **`Exercises/`** ： 习题及其解析
    1. **`Lectures/`**： 讲义（含术语解释等）
    1. **`Slides/`** ： 幻灯片/课件/PPT（使用`VSCode marp`编辑）
    1. **`README.md`** ： 本文件

## 关于课程

### 课程基本信息

1. 课程名称： 《Python程序设计》
1. 储备知识（建议）
    1. `C`： 编程语言结构、指针与内存组织
    1. `Java`： 面向对象结构、`package`
    1. `Linux`： `Command Line Interface`与运行环境
    1. `Bash`： `interactive CLI`、`Script`、脚本语言语法结构

### 课程教材与实验教材

:book: 课程教材 && 实验教材： [《Python程序设计基础与应用》](https://item.jd.com/12433472.html)，董付国 著，机械工业出版社

## Bibliographies

### Books

1. _Python Crash Course - A Hands-On Project-Based Introduction to Programming(2nd Edition)_, Eric Matthes. <https://nostarch.com/pythoncrashcourse2e>, <https://ehmatthes.github.io/pcc_2e/>, <https://github.com/ehmatthes/pcc_2e/>
1. _Python Cookbook (3rd Edition)_
    1. Chinese version: <https://github.com/yidao620c/python3-cookbook>

### Courses

1. MIT -- Introduction to Computer Science and Programming in Python: <https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-0001-introduction-to-computer-science-and-programming-in-python-fall-2016/>

### Tutorials

1. :star2: 廖雪峰的Python教程: <https://www.liaoxuefeng.com/wiki/1016959663602400>
1. 菜鸟教程 - Python3教程: <https://www.runoob.com/python3/python3-tutorial.html>
1. Python精要教程： <https://wizardforcel.gitbooks.io/python-essential-tutorial/content/>
1. PyCoder’s Weekly 中文: <https://github.com/PyCodersCN/PyCodersCN>
1. From Python to Numpy: <https://www.labri.fr/perso/nrougier/from-python-to-numpy/>
1. sklearn库的学习: <https://blog.csdn.net/u014248127/article/details/78885180>

### Documentations

1. :star2: Python 3 documentation
    1. English version: <https://docs.python.org/3/>
    1. Chinese version: <https://docs.python.org/zh-cn/3/>
1. anaconda docker: <https://docs.anaconda.com/anaconda/user-guide/tasks/docker/>
1. NumPy User Guide
    1. English version: <https://numpy.org/doc/stable/user/>
    1. Chinese version: <https://www.numpy.org.cn/user/>, <https://github.com/teadocs/numpy-cn>
1. Pandas User Guide
    1. English version: <https://pandas.pydata.org/docs/user_guide/index.html>
    1. Chinese version: <https://www.pypandas.cn/docs/>, <https://github.com/teadocs/pandas-cn>
1. Matplotlib User's Guide:
    1. English version: <https://matplotlib.org/users/index.html>
    1. Chinese version: <https://www.matplotlib.org.cn/intro/>, <https://github.com/teadocs/matplotlib-cn>
1. Scikit-Lean User Guide
    1. English version: <https://scikit-learn.org/stable/user_guide.html>
    1. Chinese version: <https://sklearn.apachecn.org/>

### Exercises

1. 100+ python programming exercise: <https://github.com/darkprinx/100-plus-Python-programming-exercises-extended>
1. Pandas Exercises: <https://github.com/guipsamora/pandas_exercises>
1. 100 numpy exercises (with solutions): <https://github.com/rougier/numpy-100>
