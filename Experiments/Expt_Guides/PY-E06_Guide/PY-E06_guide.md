# PY-E06：Python应用程序设计

## 实验基本信息

1. 实验性质：设计型
1. 实验学时：4
1. 实验目的：
    1. 了解`Python GUI`编程
    1. 了解`Python Crawler`编程
    1. 了解`Python Data Process`编程
    1. 了解`Python Data Visualization`编程
1. 实验内容与要求：
    1. 实验内容01：使用`Tkinter`实现一个定时自动关闭的窗口
    1. 实验内容02：使用`urllib`爬取公众号文章中的图片
    1. 实验内容03：使用`Pandas`模拟转盘抽奖游戏，统计不同奖项的获奖次数和获奖概率
    1. 实验内容04：使用`Matplotlib`绘制带标题、标签和图例的正弦和余弦图例
1. 实验条件：
    1. 硬件环境： `PC`
    1. 软件环境： `IDLE`或`Jupyter`或`PyCharm`

## 实验注意事项

1. 实验指引中使用 **尖括号`<>`表示键盘按键** ，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验前提条件与储备知识

## 实验指引

### 实验内容01

#### :microscope: 实验内容01具体任务

1. 程序目标： 使用`Tkinter`实现一个定时自动关闭的窗口
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan” :warning:）
        1. `And now... It is PY-E06 content 01...`

#### :ticket: 实验内容01参考截图

![](./assets_image/E0601_code.png)

#### :bookmark: 实验内容01相关知识

### 实验内容02

#### :microscope: 实验内容02具体任务

1. 程序目标： 使用`urllib`爬取公众号文章中的图片
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan” :warning:）
        1. `And now... It is PY-E06 content 02...`
    1. 公众号文章网址为：<https://mp.weixin.qq.com/s/P9Wke8FSNPxOvfLyaPcv8Q>

#### :ticket: 实验内容02参考截图

![](./assets_image/E0602_code.png)

#### :bookmark: 实验内容02相关知识

### 实验内容03

#### :microscope: 实验内容03具体任务

1. 程序目标： 使用`Pandas`模拟转盘抽奖游戏，统计不同奖项的获奖次数和获奖概率
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan” :warning:）
        1. `And now... It is PY-E06 content 03...`

#### :ticket: 实验内容03参考截图

![](./assets_image/E0603_code.png)

#### :bookmark: 实验内容03相关知识

### 实验内容04

#### :microscope: 实验内容04具体任务

1. 程序目标： 使用`Matplotlib`绘制带标题、标签和图例的正弦和余弦图例
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan” :warning:）
        1. `And now... It is PY-E06 content 04...`

#### :ticket: 实验内容04参考截图

![](./assets_image/E0604_code.png)

#### :bookmark: 实验内容04相关知识
