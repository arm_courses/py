# PY-E05：Python高级特性程序设计

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验注意事项](#实验注意事项)
3. [实验前提条件与储备知识](#实验前提条件与储备知识)
4. [实验指引](#实验指引)
    1. [实验内容01](#实验内容01)
        1. [:microscope: 实验内容01具体任务](#microscope-实验内容01具体任务)
        2. [:ticket: 实验内容01参考截图](#ticket-实验内容01参考截图)
        3. [:bookmark: 实验内容01相关知识](#bookmark-实验内容01相关知识)
    2. [实验内容02](#实验内容02)
        1. [:microscope: 实验内容02具体任务](#microscope-实验内容02具体任务)
        2. [:ticket: 实验内容02参考截图](#ticket-实验内容02参考截图)
        3. [:bookmark: 实验内容02相关知识](#bookmark-实验内容02相关知识)
    3. [实验内容03](#实验内容03)
        1. [:microscope: 实验内容03具体任务](#microscope-实验内容03具体任务)
        2. [:ticket: 实验内容03参考截图](#ticket-实验内容03参考截图)
        3. [:bookmark: 实验内容03相关知识](#bookmark-实验内容03相关知识)
    4. [实验内容04](#实验内容04)
        1. [:microscope: 实验内容04具体任务](#microscope-实验内容04具体任务)
        2. [:ticket: 实验内容04参考截图](#ticket-实验内容04参考截图)
        3. [:bookmark: 实验内容04相关知识](#bookmark-实验内容04相关知识)
    5. [实验内容05](#实验内容05)
        1. [:microscope: 实验内容05具体任务](#microscope-实验内容05具体任务)
        2. [:ticket: 实验内容05参考截图](#ticket-实验内容05参考截图)
        3. [:bookmark: 实验内容05相关知识](#bookmark-实验内容05相关知识)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：设计型
1. 实验学时：6
1. 实验目的：
    1. 掌握`Python`基本数据结构
    1. 掌握`Python`字符串
    1. 掌握`Python`正则表达式的基本用法
    1. 掌握`Python`文件内容操作
    1. 掌握`Python`文件与文件夹操作
1. 实验内容与要求：
    1. 实验内容01： 使用`set`设计一个返回指定范围内的一定数量的不重复数字的函数
    1. 实验内容02： 设计一个可逆的加解密算法
    1. 实验内容03： 提取字符串中的电话号码
    1. 实验内容04： 读取仅包含数字字符的文件内容，并将其降序排序后写入另一个文件中
    1. 实验内容05： 利用`os.walk()`遍历目录树
1. 实验条件：
    1. 硬件环境： `PC`
    1. 软件环境： `IDLE`或`Jupyter`或`PyCharm`

## 实验注意事项

1. 实验指引中使用 **尖括号`<>`表示键盘按键** ，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验前提条件与储备知识

## 实验指引

### 实验内容01

#### :microscope: 实验内容01具体任务

1. 程序目标： 使用`set`设计一个返回指定范围内的一定数量的不重复数字的函数
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan” :warning:）
        1. `And now... It is PY-E05 content 01...`
    1. 模仿`randint()`返回指定范围的一定数量的不重复数字的函数

#### :ticket: 实验内容01参考截图

![should never see me](./assets_image/E0501_code.png)

#### :bookmark: 实验内容01相关知识

### 实验内容02

#### :microscope: 实验内容02具体任务

1. 程序目标： 设计一个可逆的加解密算法
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan” :warning:）
        1. `And now... It is PY-E05 content 02...`
    1. 采用异或运算字符与密钥
    1. 循环使用密钥的字符

#### :ticket: 实验内容02参考截图

![should never see me](./assets_image/E0502_code.png)

#### :bookmark: 实验内容02相关知识

### 实验内容03

#### :microscope: 实验内容03具体任务

1. 程序目标： 提取字符串中的电话号码
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan” :warning:）
        1. `And now... It is PY-E05 content 03...`
    1. 电话号码的规则是`区号-号码`，其中`区号`是3-4位的数字字符，`号码`是7-8位的数字字符

#### :ticket: 实验内容03参考截图

![should never see me](./assets_image/E0503_code.png)

#### :bookmark: 实验内容03相关知识

### 实验内容04

#### :microscope: 实验内容04具体任务

1. 程序目标： 读取仅包含数字字符的文件内容，并将其降序排序后写入另一个文件中
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan” :warning:）
        1. `And now... It is PY-E05 content 04...`
    1. 文件`data.txt`中包含若干行整数，每行1个整数（请自行新建一个符合该规则的文本文件）
    1. 将`data.txt`内容读取，按整数数值降序排序再写入到`data_desc.txt`文件中

#### :ticket: 实验内容04参考截图

![should never see me](./assets_image/E0504_code.png)

#### :bookmark: 实验内容04相关知识

### 实验内容05

#### :microscope: 实验内容05具体任务

1. 程序目标：利用`os.walk()`遍历目录树
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的 **学号末两位** 和 **姓名全名的拼音**，如“68zhangsan” :warning:）
        1. `And now... It is PY-E05 content 05...`
    1. 设计利用`os.walk()`实现遍历目录及其子目录的函数
    1. 遍历当前目录的上一级目录

#### :ticket: 实验内容05参考截图

![should never see me](./assets_image/E0505_code.png)

#### :bookmark: 实验内容05相关知识
