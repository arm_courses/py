# PY-E02：Python分支与循环结构

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验注意事项](#实验注意事项)
3. [实验前提条件与储备知识](#实验前提条件与储备知识)
4. [实验指引](#实验指引)
    1. [实验内容01](#实验内容01)
        1. [:microscope: 实验内容01具体任务](#microscope-实验内容01具体任务)
        2. [:ticket: 实验内容01参考截图](#ticket-实验内容01参考截图)
    2. [实验内容02](#实验内容02)
        1. [:microscope: 实验内容02具体任务](#microscope-实验内容02具体任务)
        2. [:ticket: 实验内容02参考截图](#ticket-实验内容02参考截图)
        3. [:bookmark: 实验内容02相关知识](#bookmark-实验内容02相关知识)
    3. [实验内容03](#实验内容03)
        1. [:microscope: 实验内容03具体任务](#microscope-实验内容03具体任务)
        2. [:ticket: 实验内容03参考截图](#ticket-实验内容03参考截图)
        3. [:bookmark: 实验内容03相关知识](#bookmark-实验内容03相关知识)
5. [延伸阅读](#延伸阅读)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：2
1. 实验目的：
    1. 掌握`Python`的分支与循环结构
    1. 掌握使用`Python`解决具体问题
1. 实验内容与要求：
    1. 实验内容01： 求平均分
    1. 实验内容02： 判断当天为当年的第几天
    1. 实验内容03： 输出菱形图案
1. 实验条件：
    1. 硬件环境： `PC`
    1. 软件环境： `IDLE`或`Jupyter`或`PyCharm`

## 实验注意事项

1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验前提条件与储备知识

## 实验指引

### 实验内容01

#### :microscope: 实验内容01具体任务

1. 程序目标： 输入若干个成绩，求所有成绩的平均分
1. 程序要求：
    1. 在程序运行开始输出两行文字，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning:请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
        1. `And now... It is PY-E02 content 01...`
    1. 成绩只能是实数
    1. 每输完一个成绩后询问是否继续输入下一个成绩，回答“yes”继续输入，回答“no”停止输入
    1. 输出所有成绩的平均分

#### :ticket: 实验内容01参考截图

![](./assets_image/E0201_code.png)

### 实验内容02

#### :microscope: 实验内容02具体任务

1. 程序目标： 判断当天是今年的第几天中的全年天数
1. 程序要求：
    1. 在程序运行开始输出两行文字，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning:请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
        1. `And now... It is PY-E02 content 02...`
    1. 输出类似`Today is 273 of 366 in this year...`

#### :ticket: 实验内容02参考截图

![](./assets_image/E0202_code.png)

#### :bookmark: 实验内容02相关知识

### 实验内容03

#### :microscope: 实验内容03具体任务

1. 程序目标： 输出由`*`组成的菱形图案
1. 程序要求：
    1. 在程序运行开始输出两行文字，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning:请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
        1. `And now... It is PY-E02 content 03...`
    1. 用户输入一个正整数后输出由`*`组成的菱形图案

#### :ticket: 实验内容03参考截图

![](./assets_image/E0203_code.png)

#### :bookmark: 实验内容03相关知识

## 延伸阅读
