---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
backgroundColor: #202228
header: "_Programming in PY_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# _"Programming in Python"_ Overview

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Course Overview

---

### Course Basic Information

1. 课程名称： Python程序设计
1. 课程类型： 专业基础
1. 课程学分： 3
1. 课程学时： 54
1. 考核方式： 考试

---

### Course Textbooks and Resources

1. :book: 课程教材 && 实验教材： [《Python程序设计基础与应用》](https://item.jd.com/12433472.html)，董付国 著，机械工业出版社
1. :computer: aRoming's _"Programming in Python"_ resources
    1. main: <https://gitlab.com/arm_courses/py>
    1. mirror: <https://gitee.com/aroming/course_py>

---

### Course Structure

1. **part 01 overview**: chapter 01
1. **part 02 base structure (procedure-oriented programming)**: chapter 02, 04- 05
1. **part 03 object-oriented programming**: chapter 06, chapter 11
1. **part 04 advanced features**: chapter 03, 07 - 10
1. **part 05 GUI programming**: chapter 12
1. **part 06 application - network tool**: chapter 13
1. **part 07 application - data analysis**: chapter 14 - 15

---

<!--
_backgroundColor: FloralWhite
_class:
    - lead
-->

## Something about Programming Language and Python

---

### Course `C` vs `Java` vs `Python`

1. Course `C`: syntax
1. Course `Java`: syntax + environment
1. Course `Python`: syntax + environment + application

---

### Main Applications in `Python`

1. 系统管理： `Python Scripts`
1. 网络工具： `Scrapy`, `Urllib`, `Selenium`, `BeautifulSoup`
1. Web应用： `Django`, `Flask`
1. 科学应用/行业应用：
    1. 人工智能： `TensorFlow`, `PyTorch`, `Karas`
    1. 数据分析/数据可视化： `NumPy`, `Pandas`, `Matplotlib`, `SciPy`

---

### Main Applications in `Python` (contd.)

#### :point_right: `Python`以业务编程为主（即：胶水语言）

1. 面向业务的最佳通用编程语言之一
1. 性能相关部分一般由`C/C++`完成，然后封装成`Python module`

---

### Types of Programming Language by Generations

1. 1GL `machine-level`机器语言
1. 2GL `assembly`汇编语言
1. 3GL `machine-independent`高级语言: `C`, `C++`, `C#`, `Java`, `BASIC`, `Pascal`, `Fortran`, `ALGOL`, `COBOL`
1. 4GL `specific domains`查询语言: `SQL`, `Unix Shell`, `Oracle Reports`, `R`
1. 5GL `based on problem-solving`逻辑导向语言（目前主要运用于`AI`）: `OPS5`, `Mercury`

>[Programming language generations](https://en.wikipedia.org/wiki/Programming_language_generations)
---

### Types of Programming Language by Compile Time

1. 纯粹编译型语言： **运行前** 将源代码翻译成 **特定OS环境** 的机器语言/机器指令
1. 纯粹解释型语言（脚本语言）： **运行时** 将源代码翻译成 **特定OS环境** 的机器语言/机器指令（无论运行过程中是否产生中间语言）
1. 混合型语言（VM语言）： **运行前** 将源代码翻译成 **特定VM环境** 的中间语言/虚拟机指令，**运行** 时将中间语言/虚拟机指令翻译成 **特定OS环境** 下的机器语言/机器指令

---

### Types of Programming Language by Paradigm

1. 命令式： 程序员关注指示计算机如何做（`how to do`）
    1. 面向过程（冯.诺伊曼）：`Fortran`, `Pascal`, `Basic`, `C`
    1. 面向对象：`Smalltalk`, `Eiffel`, `C++`, `Java`
1. 说明式： 程序员关注让计算机做什么（`What To Do`）
    1. 函数式：`Lisp/Scheme`, `ML`, `Haskell`
    1. 数据流：`Id`, `Val`
    1. 命题逻辑式：`Prolog`, `VisiCalc`, `SQL`

---

### Types of Programming Language by Typing

1. 定义变量时是否需指定类型
    1. `static type checking静态类型`
    1. `dynamic type checking动态类型`
1. 类型转换时是否需显式转换
    1. `strongly typed强类型`
    1. `weakly typed (loosely typed)弱类型`

>1. <https://en.wikipedia.org/wiki/Type_system>
>1. <https://en.wikipedia.org/wiki/Strong_and_weak_typing>

---

### How about Python

1. 脚本语言
1. 多范式语言/多编程模型语言
1. 高抽象通用编程语言
1. 开源语言

---

### Python as Script

1. 纯粹解释型语言（优缺点分别是什么 :question: ）
1. 动态类型语言（优缺点分别是什么 :question: ）
1. 自动内存管理语言（优缺点分别是什么  :question: ）
1. 交互编程模式和脚本编程模式（支持伪编译和打包成可执行文件，可提供一定程度的保密性）

---

### Python as Multi-Paradigm

1. 命令式语言
    1. 面向过程编程
    1. 面向对象编程
1. 说明式语言
    1. 函数式编程

---

### Python as General-Purpose

1. 通用编程语言
1. 高抽象级语言
    1. 语法介于`C`/`C++`/`Java`与`Shell`之间，更接近`Shell`（优缺点分别是什么 :question: ）
    1. 适合业务系统/业务领域编程

---

### Python as Open-Source

1. 完全开源语言（`Python`流行的重要基础）
1. 丰富的第三方类库和工具（`Python`流行的最重要原因，快速解决业务问题）

---

### Father of Python

1. **`Guido van Rossum`** , who born in 31 January 1956
1. 1989(33)，created `Python` (named from `Monty Python's Flying Circus`“蒙提.派森的飞行马戏团”)
1. 2005(49)，started working at `Google`, where he spent half of his time developing the Python language
1. 2013(57)，started working at `DropBox`
1. October 2019 (63), left `Dropbox` and officially retired

> <https://en.wikipedia.org/wiki/Guido_van_Rossum>

---

#### 2006 O'Reilly Open Source Convention (OSCON)

![height:500](./assets_image/Guido_van_Rossum_OSCON_2006.jpg)

---

#### 2008 Google I/O Developer's Conference

![height:500](./assets_image/Guido_van_Rossum_at_Google_IO_2008.jpg)

---

### Short History of `Python`

1. started in December 1989, `Python` named from `Monty Python’s Flying Circus`
1. `Python 1`: released on January 1994, end of support by September 2000
1. `Python 2`: released on October 2000, end of support by January 2020
1. `Python 3`: released on 3 December 2008

> <https://en.wikipedia.org/wiki/History_of_Python>

---

### Python2

1. 2000年10月，发布`Python2.0`版，包括了更多的程序性功能、包括能自动化地管理内存的循环检测垃圾收集器，增加了对 Unicode 的支持以实现字符的标准化
1. 应各界要求，不断猛加新功能，至`Python2.6`已非常臃肿
1. 2010年07月，发布`Python2.7`版，`Python2.7`是一个过渡版
1. `Python2`于2020年失去支持（支持于2019/12/31）

---

### Python3

1. 2008年12月，发布`Python3.0`版，`Python3.0`不兼容`Python2.X`
1. `Python3`默认使用`UTF-8`编码

---

<!--
_backgroundColor: FloralWhite
_class:
    - lead
-->

## Developing with Python

---

### `Python` Develop Environments

1. `Python IDLE`(Integrated Development and Learning Environment)
1. `Eclipse PyDev`
1. `JetBrains PyCharm`
1. `Anaconda`(includes `Jupyter` and `Spyder`)

>Getting Started With Python IDLE: <https://realpython.com/python-idle/>

---

### `Python` File Extensions

1. `.py`(python)： source code, text file
1. `.pyw`(python window)：source code, text file
1. `.pyc`(python byte-code)： byte code, binary file
1. `.pyo`(python byte-code optimized)： byte code, binary file
1. `.pyd`(python depends)： depend module write by other language, binary file

---

### `pip` Install Modules

```bash {.line-numbers}
pip search
pip list
pip show
pip download
pip install [--upgrade|-U] [--force-reinstall]
    python -m pip install -U pip      # upgrade pip
pip uninstall
```

:point_right: [pip Installation](https://pip.pypa.io/en/stable/installing/): `pip` is already installed if using `Python 2 >=2.7.9` or `Python 3 >=3.4` downloaded from <https://www.python.org>

:point_right: [Python Package Index](https://pypi.org/)

---

### `conda` Install Modules

```bash {.line-numbers}
conda info
conda search <module> --info
conda search
conda list
conda install
conda uninstall         # alias for conda remove
conda remove
conda update
    conda update conda
    conda update anaconda
    conda update anaconda-navigator
    conda update xxx
conda upgrade           # alias for conda update
```

---

### `Python` vs `Anaconda` in Animal

1. python（蟒蛇）： 主要包括分布于非洲及亚洲的无毒蟒蛇
1. anaconda/green anaconda（水蚺/森蚺/水蟒/亚马逊巨蟒）： 主要包括分布于南美洲、北美洲及中美洲的一种蚺蛇
1. anaconda更重，python更长、更灵活
1. anaconda的着色图案更有序
1. anaconda喜水，python喜干
1. anaconda胎生，Python蛋生

---

### `Python Coding Style`

1. 严格缩进（语法要求）：
    1. `:`指示代码块的开始，也是缩进的开始，缩进结束代表代码块的结束
    1. 同一级别代码块的缩进量必须相同
    1. 不同级别代码块的缩进量可以不相同
    1. 一般以4个空格作为基本缩进量（将`tab`设置成4个空格）
1. 导入顺序：
    1. 每个`import`语句只导入一个模块
    1. 建议按标准库、扩展库、自定义库的顺序依次导入

---

### `Python Coding Style` (contd.)

1. 空行与空格（增加可读性）：
    1. 类、函数定义和一段完整的功能代码之后增加一个空行
    1. 运算符两侧各增加一个空格
    1. 逗号符后面增加一个空格
1. 短语句（避免超长语句）：超长语句应分拆成多个短语句
    1. 长语句用续行符（`\`）分成多行
    1. 长语句用圆括号分成多行

---

### `Python Coding Style` (contd. again)

1. 括号明确优先级
1. 使用注释： 准确表达程序逻辑或特殊说明
    1. 单行注释： 井号（`#`）开始
    1. 多行注释： 三单引号`'''`或三双引号`"""`之间

---

### `the Zen of Python`

```python {.line-numbers}
import this
```

---

### Get Helps

1. `help(obj)`
1. `print(dir(obj))`
1. :star2: <https://docs.python.org/zh-cn/3/>
    1. :star2: :star2: HTML Help (.chm) files are made available in the "Windows" section on the [Python download page](https://www.python.org/downloads/windows/)
1. :star2: <https://devdocs.io/python~3.8/>
1. <https://devhints.io/python>

---

<!--
_backgroundColor: FloralWhite
_class:
    - lead
-->

## Python模块Module in Python

---

### import

1. 导入`module`
    1. `import <module> [as <alias>]` == use as ==> `mod.obj`
1. 导入`module`中的`object`
    1. `from <module> import <object> [as <alias>]` == use as ==> `obj`
    1. `from <module> import *` == use as ==> `obj`

---

```python
import math
math.gcd(56, 64)
```

```python
import os.path as path
path.basename("/home/aroming/some_file")
```

```python
import os.makedirs #ModuleNotFoundError: No module named 'os.makedirs'; 'os' is not a package
```

---

### `__name__`

1. 以双下划线开头、双下划线结尾的变量是`Python`特殊属性： `__file__`, `__package__`, `__name__`
1. 如果作为模块被导入，则其`__name__`属性的值被自动设置为模块名
1. 如果作为程序直接运行，则其`__name__`属性值被自动设置为字符串`"__main__"`

:point_right: `python ./samples/demo_name_main.py`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Chapter :ok:
