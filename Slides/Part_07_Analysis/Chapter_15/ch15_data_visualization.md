---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 数据可视化Data Visualization

---

## Outline

1. :star2: `Matplotlib`简介
1. `Matplotlib`常用可视化图形
1. `Matplotlib`图形操作

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Matplotlib`简介

---

+ `Matplotlib`起源于模仿`MATLAB`的图形命令
+ `Matplotlib`是`Python`基本的可视化图形库
    1. `Seaborn`：基于`Matplotlib`，主要针对数据挖掘和机器学习

>[5大Python可视化库的选择](https://zhuanlan.zhihu.com/p/148748125)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Matplotlib`常用可视化图形

---

1. 折线图
1. 散点图
1. 饼状图
1. 柱状图
1. 雷达图
1. 三维曲线图
1. 三维曲面图
1. 三维柱状图

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Matplotlib`图形操作

---

1. 切分绘图区域
1. 设置图例
1. 设置坐标轴刻度距离和文本

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
