---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Data Analysis_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 数据分析Data Analysis

---

## Outline

1. `Python`数据分析概述
1. :star2: `Pandas`一维数组`Series`和二维数组`DataFrame`
1. `Pandas`结合`Matplotlib`进行数据可视化

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Python`数据分析概述

<!--TODO-->

---

1. :star2: `NumPy (Numeric Python)`： 基础数学计算库，以矩阵为主，纯数学运算库
1. :star2: `Pandas (Panel Data Analysis)`： 基于`NumPy`的数据分析与统计计算库
1. `SciPy (Science Python)`： 基于`NumPy`的科学应用计算库
1. :star2: `Matplotlib`： 基础绘图库
1. `Scikit-Learn`： 基于`SciPy`和`Matplotlib`的机器学习库

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Pandas基本操作

---

### Pandas的基本数据结构

1. `Series`： 带标签/索引的一维数组
1. `DataFrame`： 带标签/索引的且大小可变的二维数组
1. `Panel`： 带标签/索引的且大小可变的三维数组

:warning: `Panel` had deprecated since version 0.20.0 (May 5, 2017), and its api document was removed in version 0.25.0 (July 18, 2019) :warning:

:warning: `Panel`已弃用，使用`MultiIndex`的`DataFrame`替代 :warning:

---

### 一维数组`Series`

1. `Series`可以接收`list`、`tuple`、`range`、`map`等可迭代对象作为参数生成一维数组
1. `Series`和`DataFrame`默认以整数作为标签

---

```python {.line-numbers}
import numpy as np
import pandas as pd


x = pd.Series([1, 3, 5, np.nan])
print(x)
```

---

### 二维数组`DataFrame`

---

```python {.line-numbers}
class pandas.DataFrame(data=None, index=None, columns=None,
                        dtype=None, copy=False)

Parameters:
data: ndarray (structured or homogeneous), Iterable, dict, or DataFrame
index: Index or array-like
columns: Index or array-like
dtype: dtype, default None
copy: bool, default False
```

---

```python {line-numbers}
dates = pd.date_range(start='20180101', end='20181231', freq='M')
df = pd.DataFrame(np.random.randn(12, 4),
                  index=dates,
                  columns=list('ABCD'))
```

---

```python {line-numbers}
df = pd.DataFrame({'A': np.random.randint(1, 100, 4),
                   'B': pd.date_range(start='20180301', periods=4, freq='D'),
                   'C': pd.Series([1, 2, 3, 4],
                                  index=['zhang', 'li', 'zhou', 'wang'],
                                  dtype='float32'),
                   'D': np.array([3] * 4, dtype='int32'),
                   'E': pd.Categorical(["test", "train", "test", "train"]),
                   'F': 'foo'})
```

---

#### 查看`DataFrame`

1. `DataFrame.head(n = 5)`
1. `DataFrame.tail(n = 5)`
1. `DataFrame.index`
1. `DataFrame.columns`
1. `DataFrame.values`
1. `DataFrame.describe()`

---

```python {.line-numbers}
df.head()
df.tail()
df.index
df.values
df.describe()
```

---

#### 排序`DataFrame`

```python {.line-numbers}
df_sorted = df.sort_index(axis=0, ascending=False)  # 对行进行降序排序
print(df_sorted)
df_sorted = df.sort_index(axis=0, ascending=True)  # 对行进行升序排序
print(df_sorted)
df_sorted = df.sort_index(axis=1, ascending=False)  # 对列进行降序排序
print(df_sorted)
df_sorted = df.sort_values(by='A')  # 按A列进行升序排序
print(df_sorted)
'''
先按E列进行升序排序
如果E列相同，再按C列进行升序排序
'''
df_sorted = df.sort_values(by=['E', 'C'])
print(df_sorted)
```

---

#### `DataFrame`的数据选择

1. 选择`row`
    1. `DataFrame[row_index_start: row_index_end]`
    1. `DataFrame.loc[row]`
    1. `DataFrame.iloc[row]`
1. 选择`column`
    1. `DataFrame[column]`
    1. `DataFrame.column`
1. 选择`cell`
    1. `DataFrame.at[row, column]`： 使用字符串或整型
    1. `DataFrame.iat[row, column]`： 使用整型

---

```python {.line-numbers}
df['A'] # 选择某一列数据

```

---

#### `DataFrame`的条件数据选择

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
