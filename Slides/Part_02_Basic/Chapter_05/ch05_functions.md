---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 函数Function

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Define and Use Function函数的定义和使用

---

### 函数的作用

1. `encapsulate封装`： 封装细节、开放接口
1. `reuse复用`： `component组件`的最小单元（基元）

---

```python {.line-numbers}
def <func_name>([parameter list]):
    <statement block>
```

---

### 函数的定义

1. 不需要声明形参类型（有什么优缺点 :question:）
1. 不需要声明返回值类型（有什么优缺点 :question:）
    1. 未返回值时返回`None`
1. `()`圆括号和`:`冒号不能省略
1. 函数体相对于`def`必须空格缩进
1. 允许定义嵌套函数（`closure闭包`）

:exclamation: `Python 3.5`增加`typing`功能： <https://docs.python.org/zh-cn/3/library/typing.html>

---

```python {.line-numbers}
def fib(n):
    a, b = 1, 1
    while a < n:
        print(a, end='\t')
        a, b = b, a + b

fib(10)
```

---

### 函数的参数 -- 参数传递

1. `positional passing位置传参`： 实参与形参从左到右一一对应
1. `keyword passing关键字传参`： 明确指定哪个实参传递给哪个形参
1. `positional-only passing限定位置传参`： 
1. `keyword-only passing限定关键字传参`： 

:point_right: `Python 3.8`新增`positional-only passing`，`Python 3.0`新增`keyword-only passing`

---

```python {.line-numbers}
def demo_positional_pass(a, b, c):
    print(a, b, c)


def demo_keyword_pass(msg, year, month, day):
    print(f"{msg}, {year}-{month}-{day}")


demo_positional_pass(1, 2, 3)
demo_keyword_pass("hello", day=1, month=1, year=2020)
```

---

### 函数的参数 -- 形参

1. 默认形参：
    + 为形参指定默认值，不传递实参时，使用默认值
    + 默认形参右边不能再出现非默认形参（为什么 :question:）
1. 可变长度形参（`auto-packing自动装包`形参）
    1. `* para`： 将任意个实参封装成`tuple` ==> 位置传递
    1. `** para`： 将任意个实参封装成`dict` ==> 关键字传递

---

```python {.line-numbers}
def demo_default_params(msg, year, month=1, day=1):
    print(f"{msg}, {year}-{month}-{day}")


def demo_tuple_params(*iterable):
    for item in iterable:
        print(item)


def demo_dict_params(**kwarg):
    for key, value in kwarg.items():
        print(f"{key} ==> {value}")


demo_default_params("hello", 2020)
demo_tuple_params(1, 2, 3)
demo_dict_params(x=1, z=3, y=2)
```

---

### 函数的参数 -- 实参

1. `auto-unpacking`自动解包实参
    1. `* seq_argu`: 将`list`, `tuple`, `set`, `dict.keys`解包 ==> 位置传递
    1. `** dict_argu`： 将`dict`解包 ==> 关键字传递

:exclamation: `set`和`dict.keys`解包后的次序不可预测

---

```python {.line-numbers}
def demo(a, b, c):
    print(a, b, c)


a_list = [1, 3, 2]
a_tuple = (5, 7, 6)
a_set = {9, 8, 10}
a_dict = {'c': 13, 'a': 11, 'b': 12}

demo(*a_list)
demo(*a_tuple)
demo(*a_set)  # what will output
demo(*a_dict)  # what will output
demo(*a_dict.keys())  # equals to "demo(*a_dict)"
demo(*a_dict.values())  # what will output
demo(**a_dict)  # what will output

print(type(a_dict.keys()))
print(type(a_dict.values()))
```

---

### 函数的递归调用

1. `divide and conquer分而治之`
    1. 每次递归问题性质不变
    1. 每次递归问题更小
1. 明确的递归结束条件
1. 递归深度不能过大，否则可能会引起栈内存溢出

---

### 变量作用域

1. 变量作用域/变量可见域： 变量起作用的代码范围
1. 同名标识符的几种情况：
    1. 作用域无交集 ==> 各自在各自的作用域可见
    1. 作用域有交集
        1. 高优先级覆盖低优先级 ==> 高优先级可见、低优先级不可见（或显式调用低优先级标识符）
        1. 冲突 ==> `error`

---

### Python变量作用域

1. 局部变量
    1. 在函数内部定义的变量
    1. 局部变量比全局变量访问速度快（`CPython`中局部变量使用`array`存储、`index`访问，全局变量使用`dict`存储，`hash`访问）
1. 全局变量
    1. 函数中 **_只_** 引用全局变量时可不声明`global`
    1. 函数中为全局变量赋值时需使用`global`声明

---

```python {.line-numbers}
def demo_use_global_01():
    print(x)  # use global x, and x is assigned

def demo_use_global_02():
    global y
    print(y)  # use global y, but y is not assigned

x = "global object"
demo_use_global_01()
demo_use_global_02()
```

---

```python {.line-numbers}
def demo_scope1():
    x = 10
    print(x)
    global x # SyntaxError: name 'x' is used prior to global declaration
    print(x)

def demo_scope2():
    y = 10
    id(y)
    global y # SyntaxError: name 'y' is used prior to global declaration
    id(y)

y = "global object"
def demo_scope3():
    y = 10
    global another_global = "oh...another global object"
    print(y)

print(another_global) # will it OK? why?
demo_scope3()
print(y)
print(another_global)
```

---

### global声明

1. 如果全局变量已定义，且无同名局部变量
    1. 在函数内部可直接引用全局变量
    1. 在函数内部`global声明`后可赋值全局变量
1. 如果全局变量未定义，且无同名局变量
    1. 在函数内部`global声明`后可定义全局变量，该全局变量在函数执行后生效
1. 如果全局变量已定义，且有同名局部变量 ==> 不能访问全局变量

:warning: :exclamation: 函数中不能同时访问同名的全局变量和局部变量

---

:point_right: 不建议在函数内使用全局变量，如必须使用共享变量，建议使用`class`进行封装使用成员变量

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## lambda表达式

---

1. `lambda`表达式返回一个函数对象，常用于一次性使用的匿名函数场景，如，调用函数时传递函数对象
1. `lambda`表达式
    1. 使用`lambda`关键字进行定义
    1. **_只_** 包含一个表达式
    1. 表达式的结果是函数的返回值

```python {.line-numbers}
lambda <parameter list>: <one expression>
```

---

```python {.line-numbers}
f = lambda x, y, z: print(x, y, z)
f(1, 2, 3)
f(1, z = 3, y = 2)

l = [1, 2, 3, 4, 5]
print(list(map(lambda x: x + 10, l)))
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Generator生成器

---

### 生成器函数

1. `generator`
1. 包含`yield`语句的函数可以定义生成器函数
1. 调用生成器函数返回生成器对象（简称`generator生成器`）
1. 生成器执行到`yield`语句时，返回一个值并暂停或挂起后面代码的执行，下次调用生成器的`__next__()`、内置函数`next()`、`for`遍历生成器元素或其他方式显式“索要”数据时恢复执行

><https://docs.python.org/zh-cn/3/tutorial/classes.html#generators>

---

```python {.line-numbers}
def f():
    a, b = 1, 1
    while True:
        yield a
        a, b = b, a + b


one_generator = f()

print(type(one_generator))
print(id(one_generator))

for _ in range(10):
    print(one_generator.__next__(), end=' ')

print("\noutside for loop")
print(one_generator.__next__())

another_generator = f()
print(id(another_generator))
print(another_generator.__next__())
print(next(another_generator))
print(another_generator.__next__())
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Chapter :ok:
