---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Exception异常

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 异常

---

### 异常是什么

1. 异常指运行时的意外情况
1. 异常出时
    1. 处理异常（忽略并跳过引发异常的语句也是一种处理方式），按调用栈传递异常
    1. 直至调用栈底未处理异常时，停止运行

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 异常处理结构

---

### try...except...else...finally

```python {.line-numbers}
try:
    <statement block>
except <Exception_Clsname> [as objname]:
    <statement block>
[
except <Exception_Clsname> [as objname]:
    <statement block>
]
[
else:
    <statement block>  # if no exception raised, execute here 
]
[
finally:
    <statement block>  # no matter what had happened, execute here
]
```

---

```python {.line-numbers}
def demo_exception(x):
    try:
        x = int(x)
    except Exception as ex:
        print(f"Exception Encountered {ex}")
    else:
        print("x ==> {x}")
    finally:
        print("here in finally...")


demo_exception("this is not a number")
demo_exception("10")
```

---

## raise

1. `raise`主动抛出`exception`

---

```python {.line-numbers}
try:
    raise Exception('spam', 'eggs')
except Exception as inst:
    print(type(inst))  # the exception instance
    print(inst.args)  # arguments stored in .args
    print(inst)  # __str__ allows args to be printed directly,
    # but may be overridden in exception subclasses
    x, y = inst.args  # unpack args
    print('x =', x)
    print('y =', y)
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 断言与上下文管理语句

---

1. `assert`： 用于`debug`或`testing`，当`assert`的值为`false`时抛出异常
1. `with`： 上下文管理（context manager）子句

---

```python {.line-numbers}
a = 3
b = 5
try:
    assert a == b
except Exception as ex:
    print(f"exception ==> {type(ex)}")
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
