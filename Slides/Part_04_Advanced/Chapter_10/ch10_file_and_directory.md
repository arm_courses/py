---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_File and Folder_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 文件与文件夹操作File and Folder

---

## Outline

1. :star2: 标准库`os`、`os.path`、`shutil`
1. 递归遍历文件夹

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `os`模块

---

1. This module provides a portable way of using operating system dependent functionality.
1. If you just want to read or write a file see open(),
1. if you want to manipulate paths, see the os.path module,
1, and if you want to read all the lines in all the files on the command line see the fileinput module.
1. For creating temporary files and directories see the tempfile module,
1. and for high-level file and directory handling see the shutil module.

---

1. `chdir()`: 切换当前工作目录
1. `listdir()`： 返回目录中的文件和目录
1. `get_exec_path()`： 返回可执行文件的搜索路径
1. `getcwd()`： 返回当前工作目录
1. `remove(path)`： 删除指定的文件，要求用户拥有删除文件的权限，并且文件没有只读或其他特殊属性
1. `rename(src, dst)`： 重命名文件或目录，可以实现文件的移动，若目标文件已存在则抛出异常，不能跨越磁盘或分区
1. `replace(old, new)`： 重命名文件或目录，若目标文件已存在则直接覆盖，不能跨越磁盘或分区
1. `scandir()`： 返回包含指定文件夹中所有DirEntry对象的迭代对象，遍历文件夹时比listdir()更加高效

---

1. `startfile(filepath [, operation])`： 使用关联的应用程序打开指定文件或启动指定应用程序
1. `system()`： 启动外部程序

---

1. `os.curdir`： 表示当前文件夹的字符，`Windows`和`UNIX-like`均为`.`
1. `os.environ`： 包含系统环境变量和值的字典
1. `os.pathsep`： 当前`OS`的`path`环境变量分隔符
1. `os.extsep`： 当前`OS`的文件扩展名分隔符
1. `os.sep`： 当前`OS`的路径分隔符
1. `os.altsep`： 当前`OS`的可选路径分隔符，`Windows`为`/`，`UNIX-like`为`None`
1. `os.linesep`： 当前`OS`的文本文件行分隔符

---

```python {.line-numbers}
from os import listdir
from os.path import join, isfile, isdir


def list_dir_depth_first(directory):
    """深度优先遍历文件夹
        如果是文件就直接输出
        如果是文件夹，就输出显示，然后递归遍历该文件夹
    """
    for subPath in listdir(directory):
        path = join(directory, subPath)
        if isfile(path):
            print(path)
        elif isdir(path):
            print(path)
            list_dir_depth_first(path)


list_dir_depth_first("..")
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `os.path`模块

---

```python {.line-numbers}
This module implements some useful functions on pathnames
```

---

1. `abspath(path)`： 返回`path`的绝对路径
1. `basename(path)`： 返回指定路径的最后一个组成部分
1. `commonpath(paths)`： 返回给定的多个路径的最长公共路径
1. `commonprefix(paths)`： 返回给定的多个路径的最长公共前缀
1. `dirname(p)`： 返回给定路径的文件夹部分
1. `exists(path)`： 判断文件是否存在

---

1. `getatime(filename)`： 返回文件的最后访问时间
1. `getctime(filename)`： 返回文件的创建时间
1. `getmtime(filename)`： 返回文件的最后修改时间
1. `getsize(filename)`： 返回文件的大小

---

1. `isabs(path)`： 判断path是否为绝对路径
1. `isdir(path)`： 判断path是否为文件夹
1. `isfile(path)`： 判断path是否为文件

---

1. `join(path, *paths)`： 连接两个或多个path
1. `realpath(path)`： 返回给定路径的绝对路径
1. `relpath(path)`： 返回给定路径的相对路径，不能跨越磁盘驱动器或分区
1. `samefile(f1, f2)`： 测试f1和f2这两个路径是否引用的同一个文件
1. `split(path)`： 以路径中的最后一个斜线为分隔符把路径分隔成两部分，以元组形式返回
1. `splitext(path)`： 从路径中分隔文件的扩展名
1. `splitdrive(path)`： 从路径中分隔驱动器的名称

---

```python {.line-numbers}
import os

path = "mypython_exp/new_test.txt"
print(os.path.dirname(path))
print(os.path.basename(path))
print(os.path.realpath(path))  # will read filesystem
print(os.path.split(path))
print(os.path.split(""))
print(os.path.split("C:\\windows"))
print(os.path.split("C:\\windows\\"))
print(os.path.splitdrive(path))
print(os.path.splitext(path))
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `shutil`模块（shell utility）

><https://www.cnblogs.com/sui776265233/p/9225253.html>

---

```python {.line-numbers}
The shutil module offers a number of high-level operations\
on files and collections of files.\

In particular, functions are provided\
which support file copying and removal.\
For operations on individual files, see also the os module.
```

---

```python {.line-numbers}
"""Copy the contents (no metadata) of the file named src to a file named dst
and return dst in the most efficient way possible. """
shutil.copyfile(src, dst, *, follow_symlinks=True)

"""copy() copies the file data and the file’s permission mode (see os.chmod()).
Other metadata, like the file’s creation and modification times, is not preserved.
To preserve all file metadata from the original, use copy2() instead."""
shutil.copy(src, dst, *, follow_symlinks=True)

"""Identical to copy() except that copy2()
also attempts to preserve file metadata."""
shutil.copy2(src, dst, *, follow_symlinks=True)

shutil.copytree(src, dst, symlinks=False, ignore=None, copy_function=copy2, 
                        ignore_dangling_symlinks=False, dirs_exist_ok=False)

shutil.move(src, dst, copy_function=copy2)
```

---

```python {.line-numbers}
# from shutil import copyfile,copytree,ignore_patterns
import shutil

shutil.copyfile("data_01.txt", "data_01_copy.txt")
shutil.copytree("test", "test_copy", ignore=shutil.ignore_patterns("*.pyc"))
```

---

```python {.line-numbers}
shutil.make_archive(base_name, format[, root_dir[, base_dir
            [, verbose[, dry_run[, owner[, group[, logger]]]]]]])

shutil.unpack_archive(filename[, extract_dir[, format]])
```

---

```python {.line-numbers}
# from shutil import copyfile,copytree,ignore_patterns
import shutil

shutil.make_archive("../chapters", "zip", "../", "Chapters")
shutil.unpack_archive("../chapters.zip", "../chapters_unpacked")
```

---

>1. os is largely thin wrappers around POSIX system calls and library functions, or emulations thereof on some platforms. You’ll find almost identically-named functions in C and other languages, that perform analogously. The magic constants like os.R_OK, os.WNOHANG, etc are named exactly after the C/POSIX values and have the same value as them.
>1. shutil contains high-level Python-specific functions. These are built on top of Python’s os module and each call may represent dozens or hundreds of calls to lower-level functions.  
><https://www.quora.com/What-is-the-difference-between-the-OS-module-and-the-shutil-module-in-Python>

---

### `os.rename()` and `shutil.move()`

>1. OS module might fail to move a file if the source and destination path is on different file systems or drive. But shutil.move will not fail in this kind of cases.
>1. shutil.move checks if the source and destination path are on the same file system or not. But os.rename does not check, thus it fails sometimes.

---

>1. After checking the source and destination path, if it is found that they are not in the same file system, shutil.move will copy the file first to the destination. Then it will delete the file from the source file. Thus we can say shutil.move is a smarter method to move a file in Python when the source and destination path are not on the same drive or file system.
>1. shutil.move works on high-level functions, while os.rename works on lower-level functions.  
><https://www.codespeedy.com/difference-between-os-rename-and-shutil-move-in-python/>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
