---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Regular Expression_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 正则表达式Regular Expression

>1. <https://www.liaoxuefeng.com/wiki/1016959663602400/1017639890281664>
>1. <https://segmentfault.com/a/1190000022242427>
>1. <https://rgb-24bit.github.io/blog/2018/python-regex.html>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Outline

1. :star2: 正则表达式的基本语法
1. 正则表达式的扩展语法（子模式/分组）
1. :star2: `re`模块和`match`对象的基本用法

---

## 正则表达式概述

1. `regex/regexp/re (regular expression)`由`UNIX`的`sed`和`grep`普及，目前主流之一的`regex`继承自`PCRE (Perl Compatible Regular Expressions)`
1. `regex`是`text/string processing`的常用且有效工具
1. `regex`使用预定义的`pattern`去匹配将匹配的字符串，以达到复杂的查找、替换等文本处理
1. `regex`在系统管理、文本编辑与处理、网络爬虫等场合具有重要的应用价值

---

## 正则表达式工具

1. :star2: <https://regex101.com>
1. :star2: <https://jex.im/regulex/>
1. <https://regexr.com/>
1. <https://www.debuggex.com/>
1. <https://regexper.com/>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 正则表达式语法概述

---

1. `re`由元字符、字符及其组合构成
1. 以`'\'`开头的元字符与转义字符相同，因此，需使用`"\\"`或`raw string`
1. `"()"`定义一个子模式/分组，子模式作为一个整体对待，如`(red)+`表示`"red"`这个字符串重复至少1次
1. `"[]"`定义一个字符集合，字符集合中任意一个字符匹配即可
1. `"{}"`定义频次（量词）

---

```python {.line-numbers}
import re

s = "a s d"
print(re.sub("[asd]", "good", s))
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 转义字符

---

<!--TODO:improve it-->

|字符|含义|
|--|--|
|`\num`|子模式编号|
|`\f`|换页符|
|`\n`|换行符|
|`\r`|回车符|
|`\a`|报警符|

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 字符集合

---

|表达式|含义|举例|
|--|--|--|
|枚举|任意一个字符|`[abc]`|
|`-`|字符范围|`[a-z]`|
|`^`|取反|`[^a-z]`|

```python {.line-numbers}
import re

text = "alpha. beta....gamma delta"
print(re.split("[. ]+", text))
```

---

### 预定义字符集合

|字符|含义|
|--|--|
|`.`|任意字符|
|`\w`|匹配字符、数字和下划线|
|`\W`|`\w`取反|
|`\d`|匹配数字，等价于`[0-9]`||
|`\D`|`\d`取反，等价于`[^0-9]`|
|`\s`|匹配空白字符，等价于`[\f\n\r\t\v]`|
|`\S`|`\s`取反|

:warning: `Python 3`中`.`和`\w`包含`Unicode`字符

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 量词Quantifiers

---

|字符|含义|
|--|--|
|`*`|0次或者多次|
|`+`|1次或者多次，至少1次|
|`?`|0次或都1次|
|`{n,m}`|n至m次（含n次和m次）|
|`{n,}`|至少n次|
|`{n}`|刚好n次|

:warning: `{}`中的`,`前后不能有空白字符 :warning:

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 边界限定符

---

|字符|含义|
|--|--|
|`^`|行的开头|
|`$`|行的结尾|
|`\b`|单词的边界|
|`\B`|非单词的边界|
|`\A`|字符串的开头|
|`\Z`|字符串的结尾|

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 子模式/分组Sub-Pattern/Sub-Class

---

|语法|说明|举例
|--|--|--|
|`(?iLmsux)`|设置`flag`，位于模式之前|`(?i)\w+`|
|`(?:...)`|匹配但不保存该匹配|`(?:\w+)`|
|`(?P<groupname>)`|组命名|`(?P<word>\w+)`|
|`(?P=groupname)`|回溯引用|`(?p=word)`|
|`(?#...)`|注释|`(?#comment)`|

---

|语法|说明|举例
|--|--|--|
|`(?<=…)`|用于正则表达式之前，表示如果<=后的内容在字符串中不出现则匹配，但不返回<=之后的内容|
|`(?=…)`|用于正则表达式之后，表示如果=后的内容在字符串中出现则匹配，但不返回=之后的内容|
|`(?<!...)`|用于正则表达式之前，表示如果<!后的内容在字符串中不出现则匹配，但不返回<!之后的内容|
|`(?!...)`|用于正则表达式之后，表示如果!后的内容在字符串中不出现则匹配，但不返回!之后的内容|

---

```python {.line-numbers}
import re

s = "It's a very good good idea"
print(re.findall(r"(\b\w+) \1", s))
print(re.sub(r"(\b\w+) \1", r"\1", s))
s = "It's a very good good good good idea"
print(re.findall(r"((\b\w+) )\1+", s))
print(re.sub(r"((\b\w+) )\1+", r"\1", s))
print(re.sub(r"((\b\w+) )\1+", r"\2", s))
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## re模块

---

1. `re`模块定义了7个`flag`（`int`常量）、以及2个试验性的`flag`

---

|函数|说明|
|--|--|
|`findall|(pattern, string[, flags])`|返回包含字符串中所有与给定模式匹配的项的列表|
|`match(pattern, string[, flags])`|从字符串的开始处匹配模式，返回第一个匹配项封装成的`match`对象或`None`|
|`search(pattern, string[, flags])`|在整个字符串中寻找模式，返回第一个匹配项封装成`match`对象或`None`|

---

|函数|说明|
|--|--|
|`split(pattern, string[, maxsplit=0])`|根据模式匹配项分隔字符串|
|`sub(pat, repl, string[, count=0])`|将字符串中所有与pat匹配的项用repl替换，返回新字符串，repl可以是字符串或返回字符串的可调用对象，作用于每个匹配的match对象|

---

### flags

|flag|说明|
|--|--|
|`re.A`/`re.ASCII`|预定义字符集合只匹配`ASCII`字符|
|`re.U`/`re.UNICODE`|预定义字符集合可匹配`Unicode`字符|
|`re.I`/`re.IGNORECASE`|忽略大小写|
|`re.L`/`re.LOCALE`|由当前语言区域设置决定<br/>`\w`、`\W`、`\b`、`\B`和`case-insensitive`匹配|

---

|flag|说明|
|--|--|
|`re.M`/`re.MULTILINE`|多行匹配模式，`^`/`$`可匹配字符串开头/结尾以及行开头/结尾|
|`re.S`/`re.DOTALL`|使元字符`"\."`匹配任意字符，包括换行符|
|`re.X`/`re.VERBOSE`|忽略模式中的空格，并可以使用#注释|

---

### flags (contd.)

1. 不同的`flag`通过`|`运算符组合
1. `re.L`/`re.LOCALE`只能用于`bytes patterns`，官方不推荐使用
1. `re.U`/`re.UNICODE`在`Python 3`中是默认支持的`flag`（`Python 3`默认支持`Unicode`）
1. 2个试验性`flag`
    1. `re.DEBUG`： 输出打印编译`pattern`时的`debug`信息
    1. `re.TEMPLATE`： 当前官方文档未给解释

---

### `match`

1. `re.match()`和`re.search()`函数匹配成功后返回一个`match object`
1. `match`的主要方法有：
    1. `group()`：返回匹配的一个或多个子模式内容
    1. `groups()`：返回一个包含匹配的所有子模式内容的元组
    1. `groupdict()`：返回包含匹配的所有命名子模式内容的字典
    1. `start()`：返回指定子模式内容的起始位置
    1. `end()`：返回指定子模式内容的结束位置的前一个位置
    1. `span()`：返回一个包含指定子模式内容起始位置和结束位置前一个位置的元组

---

```python {.line-numbers}
import re

s = "abcabc"
print(re.search("bc", s))
print(re.match("bc", s))
print(re.match("ab", s))
print(re.findall("bc", s))
print(re.findall("(b)(c)", s))
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 第三方Regular Expression库

<https://pypi.org/project/regex/>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
