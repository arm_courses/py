---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Basic Data Structures/Collections (List, Tuple, Set and Dict)

---

## 提纲

1. 基本数据结构概述Basic Data Structures Overview
1. 列表与元组`list` and `tuple`
1. 切片与切片赋值`Slicing` and `Slice Assignment`
1. 集合与字典`set` and `dict`
1. 推导式与生成器`Comprehensions` and `Generator`
    1. `list/set/dict comprehension`
    1. `generator expression/generator function`
1. 封包与解包`Packing` and `Unpacking`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 基本数据结构概述Basic Data Structures Overview

---

![height:600](./assets_diagram/basic_collections.svg)

---

### 位置索引Position Index

1. 正向位置索引/正索引： $[0, len)$
1. 反向位置索引/负索引： $[-1, -len]$或$(0, -len]$

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 列表List/数组Array

---

### `list`概述

1. `list`是包含若干`item`的一段连续内存空间的数据结构
1. `list`的`delimiter`为`[]`，元素的`separator`为`,`
1. 空列表即没有元素的列表
1. `list`中元素的数据类型可以不相同
1. `list`中元素没有空隙（删除中间元素时后面元素自动前移）

---

### `list`的创建与删除

1. 创建列表对象
    1. 使用`=`直接将一个列表字面量赋值给变量
    1. 使用`list()`调用构造方法： 将`tuple`、`range`、`str`、`dict`、`set`或其他可迭代对象转换为列表
    1. 列表推导式
1. 删除列表对象
    1. 使用`del`关键字删除引用（:warning: 只是删除该变量与对象之间的引用，而不是删除对象本身）

---

```python {.line-numbers}
x = y = [1, 2, 3]
print(id(x))
print(id(y))

x.append(10)

print(id(x))
print(id(y))

del x
print(y)
```

---

### List元素访问

1. 正向下标访问： 从`0`（第一个元素）开始
1. 负向下标访问： 从`-1`（最后一个元素）开始

---

### List常用方法

```python {.line-numbers}
help(list)
```

---

### List运算符

1. `+`/`+=`
1. `*`/`*=`
1. `in`
1. `>`/`<`/`>=`/`<=`/`==`

:warning: `+`与`+=`的一个重要区别： `+`返回新的`list`，`+=`在原`list`上操作，同理，`*`与`*=`的区别与此相同

---

```python {.line-numbers}
x = [1, 2, 3]
x.append(4)
x.insert(0, 0)
x.extend([5, 6, 7])
x.pop()
x.pop(0)
x.remove(2)
del x[3]
x += [7, 8, 9, 10 ,11]
x.sort(key= lambda item:len(str(item)),reverse = True)
```

---

### 内置函数对List的操作

---

### 列表推导式List Comprehensions

1. 列表推导式返回一个`list`
1. 列表推导式等价于循环语句

```python {.line-numbers}
[<expression> (for <expression> in <iterable> <[if <condition>]>)+]
```

---

```python {.line-numbers}
vectors = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
flat_vector = [num for vector in vectors for num in vector]
print(flat_vector)

from random import randint

x = [randint(1, 10) for i in range(20)]
max_value = max(x)
max_in_rand = [(index, value) for index, value in enumerate(x) if value == max_value]
print(max_in_rand)
```

---

### 切片Slicing

1. `'[' [start] : [end] [:step] ']' ==> [start, end)`
    1. `start`的默认值为`0`
    1. `end`的默认值为`len(obj)`
    1. `step`的默认值为`1`
    1. 当`step`为负数时，表示反向切片（从尾到头方向），此时`start`对应元素应在`end`对应元素的右侧

---

```python {.line-numbers}
from random import randint

demo_list = [randint(1, 20) for i in range(10)]
print(f"demo list is ==> {demo_list}")
print(demo_list[::-1])
print(demo_list[::2])
print(demo_list[3:6])
print(demo_list[5:100])

demo_list[len(demo_list):] = [9]  # append [9] to demo_list
print(f"demo list is ==> {demo_list}")
demo_list[:0] = [1, 2]  # insert [1, 2] into the head of demo_list
demo_list[3: 3] = [4] # insert [4] into the index 3 of demo_list
```

---

### 切片与切片赋值Slicing and Slice Assignment

1. `slicing`： 切片作为右值时返回一个新对象（`out-of-place operation非原地操作`）
1. `slice assignment`： 切片作为左值时返回引用（`in-place operation原地操作`）

><https://stackoverflow.com/questions/10623302/how-assignment-works-with-python-list-slice/10623352#10623352>

这是否属于过度设计 :question:

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 元组Tuple

---

### `tuple`概述

1. 以`()`作为`delimiter定界符`（可选）
1. 以`,`作为`element separator元素分隔符`

```python {.line-numbers}
def demo_tuple_define():
    return 1, 2  # equivalent to (1, 2)


demo_tuple = 1, 2  # equivalent to (1, 2)
print(type(demo_tuple))
print(type(demo_tuple_define()))
```

---

### `tuple`概述(contd.)

1. 如果只有一个元素则须在该元素后增加一个`,`（为什么 :question:）
1. `tuple`的元素不可变（:warning: 元素指向的对象可变）

---

```python {.line-numbers}
demo_tuple = (1)
print(type(demo_tuple))
demo_tuple = (1,)
print(type(demo_tuple))
demo_tuple = 1,
print(type(demo_tuple))
demo_tuple = ([1, 2, 3],)
print(type(demo_tuple))
demo_tuple[0].append(4)
print(demo_tuple[0])
```

---

### 生成器表达式Generator Expression

1. 以`()`为界定符
1. 生成器表达式的返回值是`generator生成器`对象
1. 访问生成器对象的元素
    1. 转换为`list`或`tuple`
    1. 使用`__next__()`方法或内置函数`next()`
    1. 使用`for...in...`
    1. 从前往向正向访问元素
    1. 一次性访问

---

```python {.line-numbers}
demo_generator = ((i + 2) ** 2 for i in range(10))

g_tuple = tuple(demo_generator)  # transform to tuple
print(g_tuple)
g_list = list(demo_generator)  # will be empty list
print(g_list)

demo_generator = ((i + 2) ** 2 for i in range(10))

for item in demo_generator:  # 
    print(item, end='\t')
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 集合Set

---

### `set`概述

1. `set`没有`index`（`position`），不能切片
1. `set`的元素
    1. 元素不可重复
    1. 元素类型应是不可变数据类型
1. `set`的创建
    1. `{}`赋值
    1. `set()`
    1. `集合推导式set comprehension`

---

### `set`的常用方法

1. `add()`
1. `update()`
1. `pop()`: Remove and return **_an arbitrary element_** from the set. Raises KeyError if the set is empty.
1. `discard(elem)`/`remove(elem)`
1. `clear()`

---

```python {.line-numbers}
str_hello = "hello,world..."
print(len(str_hello))

set_demo = set(str_hello)
print(len(set_demo))

print(set_demo)
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 字典Dictionary

---

### 字典概述

1. `{key:value}`
1. `key`
    1. `key`应是不可变数据， 如`int`, `float`, `complex`, `str`, `tuple`等
    1. `key`不可重复
    1. `key`可作为下标进行下标访问
    1. `key`应是`Hashable`

---

### 字典的创建

1. 赋值
1. `dict()`
1. `字典推导式dict comprehension`

---

```python {.line-numbers}
demo_dict = {"server": "python.org", "database": "mysql"}
print(demo_dict)

demo_dict = dict()
print(demo_dict)

demo_dict = {}
print(demo_dict)

keys = ['a', 'b', 'c', 'd']
values = [1, 2, 3, 4]
demo_dict = dict(zip(keys, values))
print(demo_dict)

demo_dict = dict(name="Dong", age=39)
print(demo_dict)

demo_dict = dict.fromkeys(["name", "age", "sex"])
print(demo_dict)
```

---

```python {.line-numbers}
demo_dict = {"age": 39, "score": [98, 97], "name": "Dong", "sex": "male"}
print(demo_dict["age"])
print(demo_dict["address"])  # KeyError: 'address'
```

---

### 字典的常用方法

1. `get()`
1. `keys()`
1. `values()`
1. `items()`
1. `update([other])`
1. `pop()`
1. `popitem()`: Remove and return a (key, value) pair from the dictionary. Pairs are returned **_in `LIFO` order_**

---

```python {.line-numbers}
demo_dict = {"age": 39, "score": [98, 97], "name": "Dong", "sex": "male"}

print("demo_dict ===")
for item in demo_dict:
    print(str(type(item)) + "\t" + str(item))

print("demo_dict.keys() ===")
for item in demo_dict.keys():
    print(str(type(item)) + "\t" + str(item))

print("demo_dict.values() ===")
for item in demo_dict.values():
    print(str(type(item)) + "\t" + str(item))

print("demo_dict.items() ===")
for item in demo_dict.items():
    print(str(type(item)) + "\t" + str(item))
```

---

### 推导式Comprehension

```python {.line-numbers}
<expression> (for <expression> in <iterable> <[if <condition>]>)+
```

1. 列表推导式`list comprehension`
1. 集合推导式`set comprehension`
1. 字典推导式`dict comprehension`
1. ~~元组推导式tuple comprehension~~  ==> 生成器表达式`generator expression`

---

```python {.line-numbers}
vectors = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
list_vector = [num for vector in vectors for num in vector]
generator_vector = (num for vector in vectors for num in vector)
set_vector = {num for vector in vectors for num in vector}
dict_vector = {f"{i}{j}": num for i, vector in enumerate(vectors)
               for j, num in enumerate(vector)}
print(list_vector)
print(generator_vector)
print(set_vector)
print(dict_vector)

for v in generator_vector:
    print(v, end='\t')
```

---

```python {.line-numbers}
from random import randint
x = [randint(1, 10) for i in range(20)]
max_value = max(x)
max_in_rand = [(index, value) for index, value in enumerate(x)
               if value == max_value]
print(max_in_rand)

max_in_rand = ((index, value) for index, value in enumerate(x)
               if value == max_value)
print(max_in_rand)
max_in_rand = {(index, value) for index, value in enumerate(x)
               if value == max_value}
print(max_in_rand)
max_in_rand = {index: value for index, value in enumerate(x)
               if value == max_value}
print(max_in_rand)
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 封包与解包Packing and Unpacking

---

1. `封包packing`： 将多个值赋值给一个变量时，自动将这些值封装成相应类型
    1. 可变长度形参
    1. `return`多个值
1. `解包unpacking`： 将对象的多个元素同时赋值给多个变量
    1. :book: P50 `赋值符`两侧的数量须一致（:warning: 不一定）
    1. `赋值符`左侧类似于`形参parameter`，`赋值符`右侧类似于`实参argument`

---

```python {.line-numbers}
def demo(*args: tuple, **kwargs: dict) -> tuple:
    sum_of_list = 0
    sum_of_dict = 0
    print(args)  # (1, 2, 3)
    print(kwargs)  # {'four': 4, 'five': 5}
    for arg in args:
        sum_of_list += arg
    for k, v in kwargs.items():
        print(f"key => {k},value => {v}...")
        sum_of_dict += v
    return "sum_of_list", sum_of_list, "sum_of_dict", sum_of_dict


ret = demo(1, 2, 3, four=4, five=5)
print(ret)  # ('sum_of_list', 6, 'sum_of_dict', 9)
a, b, c, d = ret
print(f"a => {a}, b => {b}, c = > {c}, d => {d}")  # a => sum_of_list, b => 6, c = > sum_of_dict, d => 9
a, *ls_b = ret
print(f"a => {a}, b => {ls_b}")  # a => sum_of_list, b => [6, 'sum_of_dict', 9]
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
