---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_File Content_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 文件内容操作File Content

---

## Outline

1. :star2: 文件的概念及其分类
1. :star2: 文件内容的基本操作： `open()`, `with-statement`
1. 文本文件内容的操作
1. 二进制文件内容的操作
1. `Excel`与`Word`文件操作

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件的概念及其分类

---

1. 文件： 数据的载体，文件具有文件名
1. 按存储内容的不同，分为：
    + 文本文件`text file`： 仅存储相应字符集定义的字符数据，按字符集进行编解码
        1. `CR, LF(Carriage Return, Line Feed)` ==> `DOS`/`Windows`
        1. `LF` ==> `UNIX-like`
        1. `CR` ==> `MacOS` before `MacOS X`(1997)
    + 二进制文件`binary file`： 任意数据，按特定定义的规则编解码

:point_right: 文本文件是一种特殊的二进制文件，文本文件仅包含字符数据，而二进制文件可能包含字符和自定义二进制数据 :point_left:

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件内容基本操作

---

### 文件操作的基本流程

1. `open()`
1. `read()`/`write()`
1. `close()`

:thinking: 为什么需要`open()`和`close()` :thinking:

---

### `open()`

```python {.line-numbers}
open(file, mode='r', buffering=-1, encoding=None,
    errors=None, newline=None, closefd=True, opener=None)
    """
    Open file and return a corresponding file object.
    If the file cannot be opened, an OSError is raised.

    In text mode, if encoding is not specified the encoding used is platform dependent:
    locale.getpreferredencoding(False) is called to get the current locale encoding.

    buffering is an optional integer used to set the buffering policy.
        Pass 0 to switch buffering off (only allowed in binary mode),
        1 to select line buffering (only usable in text mode),
        and an integer > 1 to indicate the size in bytes of a fixed-size chunk buffer.
        When no buffering argument is given, the default buffering policy works
    """
```

---

#### `mode`

|Flag|Meaning|
|--|--|
|'r'(read)|open for reading (**default**)|
|'w'(write)|open for writing, truncating the file first|
|'x'(exclusive)|open for exclusive creation, failing if the file already exists|
|'a'(append)|open for writing, appending to the end of the file if it exists|
|'+'|open for updating (reading and writing)|

---

|Character|Meaning|
|--|--|
|'b'(binary)|binary mode|
|'t'(text)|text mode (**default**)|

---

### 文件对象常用方法

1. `close()`： 把缓冲区的内容写入文件，同时关闭文件，并释放文件对象
1. `flush()`： 把缓冲区的内容写入文件，但不关闭文件
1. `seek(offset[, whence])`： 把文件指针移动到新的字节位置，offset表示相对于whence的位置。whence为0表示从文件头开始计算，1表示从当前位置开始计算，2表示从文件尾开始计算，默认为0
1. `tell()`： 返回文件指针的当前位置

---

1. `read([size])`： 从文本文件中读取`size`个字符（`Python 3.x`）的内容作为结果返回，或从二进制文件中读取指定数量的字节并返回，省略`size`则表示读取所有内容
1. `readline()`： 从文本文件中读取一行内容作为结果返回
1. `readlines()`： 把文本文件中的每行文本作为一个字符串存入列表中，返回该列表
1. `write(s)`： 把`s`的内容写入文件
1. `writelines(s)`： 把字符串列表写入文本文件，不添加换行符

---

### `with-statement`

```python {.line-numbers}
with open(filename[, mode] [, encoding]) as fp:
    <statement using fp>

with context_expression [as target(s)]:
    <with-body>
```

1. 自动管理资源，保证文件被正确关闭
1. 常用于文件操作、数据库操作等场合

---

```python {.line-numbers}
with open(r"somefileName") as somefile:
    for line in somefile:
        print line
        # ...more code

try:
    somefile = open(r'somefileName')
    for line in somefile:
        print line
        # ...more code
finally:
    somefile.close()
```

><https://developer.ibm.com/zh/languages/python/articles/os-cn-pythonwith/>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文本文件内容的操作

---

```python {.line-numbers}
s = 'Hello world\n文本文件的读取方法\n文本文件的写入方法\n'

with open('sample.txt', 'w') as fp:  # 使用OS默认字符集
    fp.write(s)

with open('sample.txt') as fp:  # 使用OS默认字符集
    print(fp.read())

with open('sample.txt') as fp:
    for line in fp:  # 文件对象可迭代
        print(line)
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 二进制文件内容的操作

---

### 序列化与反序列化

>1. <https://zh.wikipedia.org/wiki/%E5%BA%8F%E5%88%97%E5%8C%96>
>1. <https://zhuanlan.zhihu.com/p/40462507>

---

+ `serialization序列化`： 将内存对象转化为可传输的字节序列（字节数组）的过程
+ `deserialization反序列化`： 将接收到的字节序列（字节数组）转化为内存对象的过程
+ 序列化和反序列化的原因是为了将内存对象`persistent持久化`（跨平台地存储和传输）
    1. `IO`支持的数据结构是字节数组
+ `serialization` = `byte array` + `protocol`
+ `Python`标准库中常用的序列化模块有： `struct`、`pickle`、`shelve`、`marshal`

---

### 序列化与反序列化的现实例子 ==> 搬迁房子

1. 将房子拆成一个个砖块（`byte array`）放到车子里，同时留下一张房子原来结构的图纸（`protocol`）
1. 将一个个砖块运输到目的地
1. 按原来结构的那张图纸将一个个砖块还原回房子原来的样子

---

### `pickle`

```python {.line-numbers}
dump()
load()
```

---

```python {.line-numbers}
import pickle

i = 13000000
a = 99.056
s = "中国人民 123abc"
lst = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
tu = (-5, 10, 8)
coll = {4, 5, 6}
dic = {'a': "apple", 'b': "banana", 'g': "grape", 'o': "orange"}
data = (i, a, s, lst, tu, coll, dic)
with open('sample_pickle.dat', 'wb') as f:
    try:
        pickle.dump(len(data), f)  # 要序列化的对象个数
        for item in data:
            pickle.dump(item, f)  # 序列化数据并写入文件
    except:
        print('写文件异常')
```

---

```python {.line-numbers}
import pickle

with open('sample_pickle.dat', 'rb') as f:
    n = pickle.load(f)  # 读出文件中的数据个数
    for i in range(n):
        x = pickle.load(f)  # 读取并反序列化每个数据
        print(x)
```

---

### `struct`

```python {.line-numbers}
pack()
write()
read()
unpack()
```

---

```python {.line-numbers}
import struct

n = 1300000000
x = 96.45
b = True
s = "a1@中国"
sn = struct.pack("if?", n, x, b)  # 序列化，i表示整数，f表示实数，?表示逻辑值
with open("sample_struct.dat", 'wb') as f:
    f.write(sn)
    f.write(s.encode())  # 字符串编码成字节串写入文件
```

---

```python {.line-numbers}
import struct

with open('sample_struct.dat', 'rb') as f:
    sn = f.read(9)
    n, x, b1 = struct.unpack('if?', sn)  # 使用指定格式反序列化
    print('n=', n, 'x=', x, 'b1=', b1)
    s = f.read(9).decode()
    print('s=', s)
```

:warning: 需知道文件中的数据类型及长度，才能正确解码 :warning:

---

### `shelve`

1. 字典赋值写入二进制文件
1. 字典读取二进制文件

```python {.line-numbers}
shelve.open()
shelve.close()
```

---

```python {.line-numbers}
import shelve

zhangsan = {"age": 38, "sex": "Male", "address": "SDIBT"}
lisi = {"age": 40, "sex": "Male", "qq": "1234567", "tel": "7654321"}
with shelve.open("sample_shelve.dat") as fp:
    fp["zhangsan"] = zhangsan
    fp["lisi"] = lisi
    for i in range(5):
        fp[str(i)] = str(i)
```

---

```python {.line-numbers}
import shelve

with shelve.open('sample_shelve.dat') as fp:
    print(fp['zhangsan'])  # 读取并显示文件内容
    print(fp['zhangsan']['age'])
    print(fp['lisi']['qq'])
    print(fp['3'])
```

---

### `marshal`

```python {.line-numbers}
dump()
load()
```

---

```python {.line-numbers}
import marshal

x1 = 30
x2 = 5.0
x3 = [1, 2, 3]
x4 = (4, 5, 6)
x5 = {"a": 1, "b": 2, "c": 3}
x6 = {7, 8, 9}
x = [eval("x" + str(i)) for i in range(1, 7)]  # eval()

with open("sample_marshal.dat", "wb") as fp:
    marshal.dump(len(x), fp)
    for item in x:
        marshal.dump(item, fp)
```

---

```python {.line-numbers}
import marshal

with open('sample_marshal.dat', 'rb') as fp:
    n = marshal.load(fp)
    for i in range(n):
        print(marshal.load(fp))
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Excel`与`Word`文件操作

---

1. 读写`Excel`： 扩展库`openpyxl`(open python excel)
1. 读写`Word`： 扩展库`python-docx`

---

### `openpyxl`

---

```python {.line-numbers}
import openpyxl
from openpyxl import Workbook

fn = "sample_openpyxl.xlsx"
wb = Workbook()  # 创建工作簿
ws = wb.create_sheet(title="你好，世界")  # 创建一个新的工作表

for sheet_name in wb.sheetnames:
    print(sheet_name, end='\t')

ws["A1"] = "这是第一个单元格"  # 单元格赋值
ws["B1"] = 3.1415926
wb.save(fn)  # 保存Excel文件
```

---

```python {.line-numbers}
import openpyxl
from openpyxl import Workbook

wb = openpyxl.load_workbook(fn)  # 打开已有的Excel文件
ws = wb.worksheets[1]  # 打开指定索引的工作表
print(ws["A1"].value)  # 读取指定单元格的值
ws.append([1, 2, 3, 4, 5])  # 添加一行数据
ws.merge_cells("F2:F3")  # 合并单元格
ws["F2"] = "=sum(A2:E2)"  # 写入公式
for r in range(10, 15):
    for c in range(3, 8):
        ws.cell(row=r, column=c, value=r * c)  # 写入单元格数据
wb.save(fn)
```

---

### `python-docx`

---

```python {.line-numbers}
from docx import Document

doc = Document('sample_python-docx.docx')

contents = ''.join((p.text for p in doc.paragraphs))
words = []
for index, ch in enumerate(contents[:-2]):
    if ch == contents[index + 1] or ch == contents[index + 2]:
        word = contents[index:index + 3]
        if word not in words:
            words.append(word)
            print(word)
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
