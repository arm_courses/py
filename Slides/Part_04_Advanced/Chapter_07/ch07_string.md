---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 字符串String

---

## 提纲Outline

1. 字符编码
1. 转义字符
1. 字符串格式化
1. 字符串常用方法
1. 字符串运算符
1. 内置函数的字符串操作

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 字符串概述

---

1. `class str`
1. 不可变容器结构
    1. 不能进行元素添加、修改与删除操作
    1. `str`的方法返回新的`str`
1. 使用`'...'`/`"..."`/`'''...'''`/`"""..."""`作为`delimiter`，不同的`delimiter`可相互嵌套

:point_right: `Python`多行注释也使用`'''...'''`/`"""..."""`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 字符编码

>1. <https://en.wikipedia.org/wiki/Unicode>
>1. <https://zhuanlan.zhihu.com/p/89635772>
>1. <https://www.liaoxuefeng.com/wiki/1016959663602400/1017075323632896>
>1. <https://www.cnblogs.com/malecrab/p/5300503.html>
>1. <http://godsing.top/2018/01/21/%E4%B8%AD%E6%96%87%E5%AD%97%E7%AC%A6%E9%9B%86%E7%BC%96%E7%A0%81GB2312%E3%80%81GBK(CP936)%E3%80%81GB18030/>
>1. <https://convertcodes.com/>

---

1. `ASCII`： 单字节编码
1. `GB2312`/`GBK`/`CP936`/`GB18030`： 双字节编码（英文单字节、中文双字节）
1. `Unicode`： 编码标准，具体`mapping and encoding`与实现方式相关
    1. `UTF-8(Unicode Transformation Format)`： 多字节编码，`Unicode`的一种存储方式
    1. `UCS-2(Universal Coded Character Set)`： 双字节编码，通常用于内码
1. 字符长度 != 字节长度

---

1. `Python`内码使用`UCS-2`或`UCS-4`，源文件默认使用`UTF-8`

```python {.line-numbers}
import sys

"""
Before PEP 393(https://www.python.org/dev/peps/pep-0393/),
sys.maxunicode used to be either 0xFFFF or 0x10FFFF(1114111), 
depending on the configuration option that specified 
whether Unicode characters were stored as UCS-2 or UCS-4.
"""
print(sys.maxunicode)
```

---

### `encode()` and `decode()`

```python {.line-numbers}
str.encode(encoding="utf-8", errors="strict") -> bytes
    Return an encoded version of the string as a bytes object.

bytes.decode(encoding="utf-8", errors="strict") -> str
bytearray.decode(encoding="utf-8", errors="strict") -> str
    Return a string decoded from the given bytes.
```

---

```python {.line-numbers}
s_hello_en = "hello"
b_hello_en = s_hello_en.encode()

s_hello_cn = "您好"
b_hello_cn = s_hello_cn.encode()

print(s_hello_en)
print(b_hello_en)
print(s_hello_cn)
print(b_hello_cn)

print(b_hello_en.decode())
print(b_hello_cn.decode())
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 转义字符Escape Character

---

|转义字符|含义|转义字符|含义|
|--|--|--|--|
|`\b`|退格符|`\\`|反斜杠|
|`\f`|换页符|`\'`|单引号|
|`\n`|换行符|`\"`|双引号|
|`\r`|回车符|`\ooo`|3位8进制数对应的字符|
|`\t`|水平制表符|`\xhh`|2位16进制数对应的字符|
|`\v`|垂直制表符|`\uhhhh`|4位16进制数表示的`UCS-2`字符|

---

### 原始字符串`Raw String`

1. 在 **_字符串字面量_** 前加`r`或`R`表示原始字符串，即，字符串中的字符均表示原始的含义而不进行转义
1. `raw string`主要用于`Windows`路径和`Regular Expression`

```python {.line-numbers}
path = "C:\Windows\notepad.exe"
print(path)
path = "C:\\Windows\\notepad.exe"
path_raw = r"C:\Windows\notepad.exe"
assert(path == path_raw)
```

:warning: `raw string`只是字符串字面量的语法糖

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 字符串格式化String Format

---

1. `%-formatting`： 最早支持的字符串格式化方式
1. `str.format()`： `Python 2.6`新增（2008年）
1. `formatted string`： `Python 3.6`新增（2016年）

>1. <https://medium.com/@zoejoyuliao/%E6%AF%94%E8%BC%83-python-%E7%9A%84%E6%A0%BC%E5%BC%8F%E5%8C%96%E5%AD%97%E4%B8%B2-formatting-str-format-f-string-6d28487ba1d2>
>1. <https://pyformat.info/>

---

## `%-formatting`

1. 与`C`的`sscanf()`类似
1. 通过`%`将`format string template`与`tuple`（实参）相关联
1. 按位置一一对应（位置传参）

```python {.line-numbers}
a, b = 2, 3

format_template = "%d/%d = %f"
print(format_template % (a, b, a / b))
print("%d/%d = %f" % (a, b, a / b))
```

---

```python {.line-numbers}
"%[-] [+] [0] [m] [.n] <format>"%(<*args>)

-: 左对齐
+: 正数正号
0: 空位补0
m: 最小宽度
.n: 精度
format: 格式符
(<*args>): 待转换的`tuple`
```

---

|格式符|说明
|--|--|
|`%s`|字符串 (显示`str()`的返回值)|
|`%r`|字符串 (显示`repr()`的返回值)|
|`%c`|单个字符|
|`%%`|字符`%`|

---

|格式符|说明
|--|--|
|`%d`|十进制整数|
|`%i`|十进制整数|
|`%o`|八进制整数|
|`%x`|十六进制整数|
|`%e`|指数（基底写为`e`）|
|`%E`|指数（基底写为`E`）|
|`%f`/`%F`|浮点数|
|`%g`|指数（`e`）或浮点数（根据显示长度）|
|`%G`|指数（`E`）或浮点数（根据显示长度）|

---

### `str.format()`

1. 通过`{}`和`:`代替`%`运算符
    1. `{}`： 形参`delimiter`
    1. `:`： 左边为形参，右边为格式符
1. 除`%`支持的格式外，还支持：
    1. `align对齐`： `^`（居中对齐）、`<`（左对齐）、`>`（右对齐）
    1. `thousands separator千位分隔符`： `,`（逗号）、`_`（下划线）
    1. `fill填充符`： `#`（进制前导字符）

---

```python {.line-numbers}
{<param>:<[fill]><[align]><[width]><[thousands separator]><[.precision]><[data type]>}
```

```python {.line-numbers}
a, b = 2, 3

str_format_template = "{0:^6}/{b:#x} = {c:f}"
print(str_format_template.format(a, b=b, c=a / b))
print("{0:^6}/{b:#x} = {c:f}".format(a, b=b, c=a / b))
```

---

### `f-string`

1. **_字符串字面量_** 前加前缀`f`，格式符与`str.format()`类似
1. `f-string`不能先定义`string format template`再定义`variable`（`%-format`和`str.format()`可以）
1. `f-string`只能使用关键字传参，不能使用位置传参

```python {.line-numbers}
a, b = 2, 3

print(f"{a}/{b:#x}={a / b:+6}")
```

---

```python {.line-numbers}
a, b = 2, 3

format_template = "%d/%d = %f"
print(format_template % (a, b, a / b))
print("%d/%d = %f" % (a, b, a / b))

str_format_template = "{0:^6}/{b:#x} = {c:f}"
print(str_format_template.format(a, b=b, c=a / b))
print("{0:^6}/{b:#x} = {c:f}".format(a, b=b, c=a / b))

print(f"{a}/{b:#x}={a / b:+6}")
```

---

### `%-formatting` vs `str.format()` vs `f-string`

1. `string formate template`一次性使用，优先使用`f-string`
1. `string formate template`多次使用，优先使用`str.format()`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 字符串常用方法与运算符

---

1. `str`属于不可变对象，所有涉及`str`修改的方法均是返回修改后的新的`str`对象

---

### 常用方法

1. `find()`/`rfind()`
1. `index()`/`rindex()`
1. `count()`
1. `split()`/`rsplit()`
1. `partition()`/`rpartition()`

:point_right: `r`前缀代表`reversed`

:warning: `split()`/`rsplit()`在默认分隔符（空白字符）和指定分隔符时对连续出现的分隔符处理方式不一样，如果在指定分隔符时需将连续出现的分隔符当成一个分隔符，可以使用正则表达式

---

1. `join()`
1. `lower()`/`upper()`/`capitalize()`/`title()`/`swapcase()`
1. `replace()`
1. `static maketrans()`/`translate()`
1. `strip()`/`rstrip()`/`lstrip()`
1. `startswith()`/`endswith()`
1. `isalnum()`/`isalpha()`/`isdigit()`/`isspace()`/`isupper()`/`islower()`
1. `center()`/`ljust()`/`rjust()`

---

```python {.line-numbers}
trans_table = str.maketrans('abcdef123', 'uvwxyz@#$')
s = "Python is a greate programming language. I like it!"
print(s.translate(trans_table))
```

---

```python {.line-numbers}
import string


class KaiSaEncryptor:
    def __init__(self, k):
        lower = string.ascii_lowercase  # 小写字母
        upper = string.ascii_uppercase  # 大写字母
        before = string.ascii_letters
        after = lower[k:] + lower[:k] + upper[k:] + upper[:k]
        self.__encrypt_table = str.maketrans(before, after)
        self.__decrypt_table = str.maketrans(after, before)

    def encrypt(self, original_str):
        return original_str.translate(self.__encrypt_table)

    def decrypt(self, encrpyted_str):
        return encrpyted_str.translate(self.__decrypt_table)
```

---

```python {.line-numbers}
s = "Python is a greate programming language. I like it!"

kaisa_encrpyptor = KaiSaEncryptor(3)

s_encrypted = kaisa_encrpyptor.encrypt(s)
s_decrypted = kaisa_encrpyptor.decrypt(s_encrypted)

print(s_encrypted)
print(s_decrypted)
```

---

### 运算符

1. `+`
1. `*`
1. `in`

---

### 内置函数和切片

1. `len()`
1. `max()`/`min()`
1. `zip()`
1. `sorted()`
1. `eval()`
1. slice

---

### 标准库string

```python {.line-numbers}
import string

print(dir(string))
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## string第三方扩展库

---

1. `jieba`: Chinese text segmentation, <https://pypi.org/project/jieba/>
1. `SnowNLP`: Simplified Chinese Text Processing, <https://pypi.org/project/snownlp/>
1. `pypinyin`: 将汉字转为拼音，可以用于汉字注音、排序、检索，<https://pypi.org/project/pypinyin/>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
