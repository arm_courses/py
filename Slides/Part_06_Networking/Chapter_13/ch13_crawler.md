---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 网络爬虫Web Crawler

---

## Outline

1. :star2: `Web`基础技术： `HTML`、`CSS`与`JavaScript`
1. 爬虫应用模块`urllib`, `scrapy`, `BeautifulSoup`, `requests`, `selenium`
    1. :star2: 了解`Python`爬虫应用模块
    1. 使用`Python`爬虫应用模块

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `HTML`、`CSS`与`JavaScript`

---

+ `Web`发展简史
    1. 技术栈时代： `HTML`、`CSS`、`JavaScript`
    1. 封装库时代： `JQuery`
    1. 组件库时代： `Angular`(`Google`), `React`(`Facebook`)、`VUE`（`尤雨溪`）
+ `HTML` ==> 内容
+ `CSS` ==> 排版
+ `JavaScript` ==> 交互（人机交互、`client-server`交互）

><https://baike.baidu.com/item/%E5%B0%A4%E9%9B%A8%E6%BA%AA/2281470>

---

### `HTML`标签

1. `h`(heading): `h1` ~ `h6`
1. `p`(paragraph)
1. `a`(anchor)
1. `img`(image)
1. `table`, `tr`(table row), `td`(table data)
1. `ul`(unordered list), `ol`(ordered list), `li`(list item)
1. `div`(division)

---

### 解析`HTML`

1. `DOM(Document Object Model)`: 
    1. `xpath(XML path)`
1. `SAX(Simple API for XML)`: event-driven

>1. [Simple API for XML](https://en.wikipedia.org/wiki/Simple_API_for_XML)
>1. [xpath路径表达式笔记](https://www.ruanyifeng.com/blog/2009/07/xpath_path_expressions.html)
>1. [Java操作XML](https://zhuanlan.zhihu.com/p/80407331)

---

### `JavaScript`

>1. [廖雪峰：JavaScript教程](https://www.liaoxuefeng.com/wiki/1022910821149312)
>1. [阮一峰：JavaScript教程](https://wangdoc.com/javascript/)
>1. [阮一峰：ECMAScript 6 教程](https://wangdoc.com/es6/)

---

+ 由 **_浏览器引擎_** **_解释执行_** 的 **_弱类型_** **_脚本语言_**
+ 三种代码书写方式
    1. 行内方式
    1. 页内方式
    1. 外部方式

><https://www.w3cschool.cn/javascript/js-howto.html>

---

#### `JavaScript`常用事件

+ `event-driven`
+ `onXXX`

---

#### `JavaScript`常用对象

1. `navigator`
1. `window`
1. `location`
1. `history`
1. `document`
1. `image`
1. `form`

---

### `CSS`

><https://www.w3cschool.cn/css/>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `urllib`(Standard Library)

---

1. `urllib.request`
1. `urllib.response`
1. `urllib.parse`
1. `urllib.error`

---

```python {.line-numbers}
import urllib.request

with urllib.request.urlopen(r"http://www.python.org") as fp:
    print(fp.read(100))
    print(fp.read(100).decode())
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `scrapy`(Framework)

---

+ 爬虫框架
+ 编程过程
    1. `pip install scrapy`
    1. `scrapy startproject <project_name>`
    1. coding
    1. `scrapy crawl <project_name>`

<!--
>[在PyCharm中开发、调试Scrapy应用](https://www.mls-tech.info/python/python-debug-scrapy-in-pycharm/)
-->

---

```python {.line-numbers}
import scrapy


class MySpider(scrapy.spiders.Spider):
    # 爬虫的名字，每个爬虫必须有不同的名字
    name = 'spiderYichangGuishi'
    # 要爬取的小说首页，第一篇
    start_urls = ['http://bbs.tianya.cn/post-16-1126849-1.shtml']

    # 对每个要爬取的页面，会自动调用下面这个方法
    def parse(self, response):
        # 用来存放当前页中的小说正文
        content = []
        for i in response.xpath('//div'):
            # 只爬取作者发出的内容
            if i.xpath('@_hostid').extract() == ['13357319']:
                for j in i.xpath('div//div'):
                    # 提取文本，过滤干扰符号
                    c = j.xpath('text()').extract()
                    # \u3000 => 全角空白符，属于CJK字符的CJK标点符号区块内
                    g = lambda x: x.strip('\n\r\u3000').replace('<br>', '\n').replace('|', '')
                    c = '\n'.join(map(g, c)).strip()
                    content.append(c)
        with open('result.txt', 'a+', encoding='utf8') as fp:
            fp.writelines(content)

        url = response.url  # 获取下一页网址并继续爬取
        d = url[url.rindex('-') + 1:url.rindex('.')]
        u = 'http://bbs.tianya.cn/post-16-1126849-{0}.shtml'
        next_url = u.format(int(d) + 1)
        try:
            yield scrapy.Request(url=next_url,
                                 callback=self.parse)
        except:
            pass
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `BeautifulSoup`(Extension Package)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `requests`(Extension Package)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `selenium`(Extension Package)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
